<?php

// Add post thumbnails feature
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
}
if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'single-image', 520, 670 );
	add_image_size( 'project-thumb', 56, 28 );
	add_image_size( 'carousel-thumb', 78, 100 );
}

	
// Add default posts and comments RSS feed links to head
add_theme_support( 'automatic-feed-links' );
	

	// Load jQuery and enqueue Javascript
	if ( !is_admin() ) {
	   wp_deregister_script('jquery');
	   wp_deregister_script('thickbox');//use custom thickbox for better layout control
	   wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"), false);
	   wp_register_script( 'scrollpane', get_bloginfo('template_directory').'/js/jScrollPane.js', array( 'jquery' ) );
	   wp_register_script( 'colorbox', get_bloginfo('template_directory').'/js/jquery.colorbox-min.js', array( 'jquery' ) );
	   wp_register_script( 'scrollpane_mousewheel', get_bloginfo('template_directory').'/js/jquery.mousewheel.js', array( 'jquery', 'scrollpane' ) );
		 wp_register_script('imgpreload', get_bloginfo('template_directory') . '/js/jquery.imgpreload.min.js', array('jquery') );
		 wp_register_script('global_script', get_bloginfo('template_directory') . '/js/global.js', array('jquery') );
		 wp_enqueue_script('imgpreload');
		 wp_enqueue_script('global_script');
		 wp_enqueue_script('colorbox');
	   wp_enqueue_script( 'scrollpane-init', get_bloginfo('template_directory').'/js/jScrollPane.init.js', array( 'scrollpane','scrollpane_mousewheel' ) );
	   wp_enqueue_script('jquery');
	}

	
	// remove junk from head
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);	



/* Taken from the is-child plugin */

if( !function_exists( "is_child" ) ) {
	
	function is_child( $ofParent, $doRecursive = true ) {

		global $wpdb;
		$allCats = array();
		
		// Turn title or slug into ID if needed.
		if( !is_int( $ofParent ) ) {
			if( is_page() )
				# Different handling for Pages
				$getID = $wpdb->get_results("
					SELECT	ID as cat_ID
					FROM	{$wpdb->posts}
					WHERE	post_title = '{$ofParent}'
					OR		post_name = '{$ofParent}'
					LIMIT	0,1
				");
			else
				# Get catID
				$getID = $wpdb->get_results("
					SELECT	cat_ID
					FROM	{$wpdb->categories}
					WHERE	cat_name = '{$ofParent}'
					OR		category_nicename = '{$ofParent}'
					LIMIT	0,1
				");

			if( !$getID )
				# Not found.
				return false;
			else {
				# Found.
				$ofParent = $getID[0]->cat_ID;
				unset( $getID );
			}
		}
		
		// Everyone's a sub zero.
		if( $ofParent == 0 && $doRecursive )
			return true;
	    
		// Now let's break it down to categories (or pages).
		if( is_page() ) {
			global $post;
			$allCats[] = $post->ID;
		} elseif( is_single() ) {
			$getCats = get_the_category();
			foreach( $getCats as $getCat )
				$allCats[] = $getCat->cat_ID;
			unset( $getCats );
		} elseif( is_category ) {
			global $cat;
			$allCats[] = $cat;
		}

		// Already a match? Would save processing time.
		if( in_array( $ofParent, $allCats ) )
			return true;
			
		// Post without recursive search ends here.
		if( ( is_single() ) && !$doRecursive )
			return false;

		// Otherwise, let's do some genealogy.
		while( count( $allCats ) != 0 ) {
			if( in_array( $ofParent, $allCats ) )
				return true;
			else 
				$allCats = 
					is_child_getParents( $allCats );
		}
		
		// Still here? Then nothing has been found.
		return false;

	}
}


if( !function_exists( "is_child_getParents" ) ) {

	function is_child_getParents( $fromChilds ) {
		
		// As there's only get_category_parents which isn't useful 
		// for fetching parental data, we'll have to query this
		// directly to the DB.
		global $wpdb;
		
		$fromChilds = implode( ", ", $fromChilds );
		if( !$fromChilds ) return array();
		
		$getParents = 
			( is_page() )
			?	# Pages
				$wpdb->get_results("
					SELECT	post_parent AS category_parent
					FROM	{$wpdb->posts}
					WHERE	ID IN ({$fromChilds})
				")
			: 	# Posts / Categories
				$wpdb->get_results("
					SELECT	category_parent
					FROM	{$wpdb->categories}
					WHERE	cat_ID IN ({$fromChilds})
				");
		
		foreach( $getParents as $getParent )
			if( $getParent->category_parent != 0 )
				$allParents[] = $getParent->category_parent;
			
		return $allParents;

	}
}

function get_thumbnail_nav($postType) {
  
	$args = array(
	'post_type'   => $postType,
	'title_li'     => '',
	'numberposts' => '-1',
	'order'	=> 'ASC',
	'orderby'	=> 'menu_order'
	);
	
	$pages = get_posts( $args );
	
	$output = '';
	foreach($pages as $value){
		$firstName = get_post_meta($value->ID, 'first-name', true);
		$thumb = get_the_post_thumbnail( $value->ID, array(78,78), $attr = '' );
		if ($value->ID == $current_id) {
			$output .= '<li class="active">';
		} else {
			$output .= "<li>";
		}
		if ($firstName != '') $output .= "<h4>" .  $firstName . "</h4>";
		$output .= "<a href='" . get_permalink($value->ID) . "' >" . $thumb . "</a>";
		$output .= "</li>";
	} 
	
	echo $output;
}



function get_post_images($thumbnails=false) {
	global $post;
	//Get the featured image first so we guarantee it's first in the list
	$args1 = array(
	'post_parent' => $post->ID, 
	'post_status' => 'inherit', 
	'post_type' => 'attachment', 
	'post_mime_type' => 'image', 
	'order' => 'ASC', 
	'orderby' => 'menu_order ID',
	'include' => get_post_thumbnail_id()
	);
	$photos = get_children($args1);
	
	//Get the rest of the images attached excluding the thumbnail we already got
	$args2 = array(
	'post_parent' => $post->ID, 
	'post_status' => 'inherit', 
	'post_type' => 'attachment', 
	'post_mime_type' => 'image', 
	'order' => 'ASC', 
	'orderby' => 'menu_order ID',
	'exclude' => get_post_thumbnail_id()
	);
	$photos = array_merge($photos, get_children($args2)); //merge first results array with second


	if ($photos) {
		$output = '<ul>'.PHP_EOL;
		$first = true;
		foreach ($photos as $photo) {
			$thumb = wp_get_attachment_image($photo->ID, 'carousel-thumb');
			$link = wp_get_attachment_image_src($photo->ID, 'single-image');
			$caption = get_post($photo->ID)->post_excerpt;
			$fullLink = wp_get_attachment_image_src($photo->ID, 'full');
			if($first) {
				$output .= '<li class="active">';
				$first = false;
			} else {
				$output .= "<li>";
			}
			if ($thumbnails) {
				$output .= '<a rel="' . $fullLink[0] . '" href="' . $link[0] . '" >' . $thumb . '</a>';
			} else {
				$output .= '<a href="' . $fullLink[0] . '" title="'.$caption.'"><img rel="project-gallery" src="' . $link[0] . '" ></a>';				
			}
			$output .= "</li>".PHP_EOL;
		}
		$output .= '</ul>'.PHP_EOL;
		echo $output;
	}
	

}


function get_first_post_link($customType) {
	
	$args = array (
	'post_type' => $customType,
	'order' => 'ASC',
	'orderby' => 'menu_order'
  );
	
	$myposts = get_posts($args);
	
	return get_permalink($myposts[0]->ID);
}



function get_projects($isImageList=false) {
	global $post;
	$currentPostName = $post->post_name;
	if (!$isImageList) {
		$postTerms = get_the_terms( $post->ID, 'project_sector');
	//echo '<div class="listpane">';
	}
	echo '<ul id="project-sector-list">';
  $my_terms = get_terms('project_sector', 'orderby=name&hide_empty=0');
	foreach ($my_terms as $my_term) { 
		if ($my_term->slug!='featured') {
			$addClass = '';
			if (!$isImageList) {
				foreach ($postTerms as $postTerm) {
					if ($postTerm->slug==$my_term->slug) $addClass=' active';
				}
			}
			$termlink = '';
			$termlinkClose = '';
			if (!$isImageList) { 
				$termlink = '<a href="/sectors/'. $my_term->slug .'">'; 
				$termlinkClose = '</a>';
			}
			echo '	<li class="project-sector'.$addClass.'" id="project-sector-'. $my_term->slug .'">';
			echo '		<h3>'.$termlink. $my_term->name .$termlinkClose.'<span class="project-name">&nbsp;</span></h3>';
			if (!$isImageList) echo '<div class="listpane">';
			echo '		<ul class="project-list">';
							$args=array(
								'project_sector' => $my_term->name,
								'post_type' => 'project',
								'orderby' => 'menu_order'
							);
							$my_query = null;
							$my_query = new WP_Query($args);
							if( $my_query->have_posts() ) {
								$firstItem = ' first';
								while ($my_query->have_posts()) : $my_query->the_post() ;
									$addClass='';
									if( $my_query->post->post_name==$currentPostName) $addClass=' active';
									echo '			<li class="project'.$firstItem.$addClass.'" id="project-'. $my_query->post->post_name .'"><a title="'. $my_query->post->post_title .'" href="'. get_permalink($my_query->post->ID) .'">';
									if ($isImageList) {
										echo get_the_post_thumbnail( $value->ID, array(56,112), $attr = '' );
									}
									else {
										echo $my_query->post->post_title;
									}				
									echo '</a> </li>'.PHP_EOL;
									if ($firstItem != '')  $firstItem = '';
								endwhile;
							
							}	
									
							wp_reset_query();
			echo '		</ul>'.PHP_EOL;
			if (!$isImageList) echo '</div>';
			echo '	</li>'.PHP_EOL;
		}
	 } 
	echo '</ul>'.PHP_EOL;
	//if (!$isImageList) echo '</div>';
}


function get_featured() {
echo '		<ul class="featured-list">';
						$args=array(
							'project_sector' => 'Featured',
							'post_type' => 'project',
							'orderby' => 'menu_order'
						);
						$my_query = null;
						$my_query = new WP_Query($args);
						if( $my_query->have_posts() ) {
							$firstItem = ' first';
							while ($my_query->have_posts()) : $my_query->the_post() ;
								$addClass='';
								if( $my_query->post->post_name==$currentPostName) $addClass=' active';
								echo '			<li class="project'.$firstItem.$addClass.'" id="project-'. $my_query->post->post_name .'"><a title="'. $my_query->post->post_title .'" href="'. get_permalink($my_query->post->ID) .'">';
								if ($isImageList) {
									echo get_the_post_thumbnail( $value->ID, array(56,112), $attr = '' );
								}
								else {
									echo $my_query->post->post_title;
								}				
								echo '</a> </li>';
								if ($firstItem != '')  $firstItem = '';
							endwhile;
						
						}	
								
						wp_reset_query();
		echo '		</ul>';
}

$rand_ID = '';

function get_home_image() {

	global $post;
	global $rand_ID;
	
	//Get the images attached excluding the featured image
	$args = array(
	'post_parent' => $post->ID, 
	'post_status' => 'inherit', 
	'post_type' => 'attachment', 
	'post_mime_type' => 'image', 
	'order' => 'ASC', 
	'orderby' => 'menu_order ID',
	'exclude' => get_post_thumbnail_id()
	);
	$photos =  get_children($args);

	$output = '';
	
	if ($photos) {
		
		// First, build the photo list
	
			
		$count = 0;
		$imageIDs = array();
		
		foreach ($photos as $photo) {
			$link = wp_get_attachment_image_src($photo->ID, 'full');
			if (strlen(strstr($link[0],'_stretch'))==0) {	
				$imageIDs[$count] = $photo->ID;
				$count ++;
			}
		}
		$rand_ID = $imageIDs[array_rand($imageIDs, 1)];
		$imgSrc =  wp_get_attachment_image_src($rand_ID, 'full');
		$popImgSrc =  str_replace('.jpg','_stretch.jpg',$imgSrc[0]);
		$imgCaption =  get_post($rand_ID)->post_excerpt;
		$output .= '<img id="popImage" src="'.$popImgSrc.'" />';
		echo $output;
		return $imgCaption;
	}
		
}

function get_tabs() {

	global $rand_ID;
	global $wpdb;
	
	$featuredLink = get_post($rand_ID)->post_content;
	$featuredTitle = get_post($rand_ID)->post_title;
	
	
	
	$activityLink = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post'  ORDER BY RAND() LIMIT 1");
	$activityTitle = get_post($activityLink)->post_title;
	
	$output =  '<ul id="tab-list">'.PHP_EOL;
	//$output .= '	<li><a href="studio/activity/#post-'.$activityLink.'">Activity: <span>'.$activityTitle.'</span></a></li>'.PHP_EOL;
	$output .= '	<li><a href="'.$featuredLink.'">Feature Project: <span>'.$featuredTitle.'</span></a></li>'.PHP_EOL;
	$output .= '</ul>';
	
	echo $output;
}



/* Remove Comments and Links from Admin menu items */
	function remove_menu_items() {
		global $menu;
		$restricted = array(__('Comments'),__('Links'));
		end ($menu);
		while (prev($menu)){
			$value = explode(' ',$menu[key($menu)][0]);
			if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){
				unset($menu[key($menu)]);}
			}
	}

	add_action('admin_menu', 'remove_menu_items');

