$(document).ready( function(){
	if ($('.home').length>0) bkgPopup();
	if ($('.page-template-play-php').length>0) playBkgPopup();
	//if ($('.single-team-member').length>0) carouselScroller(); 
	/*if ($('.home').length==0) $('#wrap').fadeIn(500, function() {
		if ($('.parent-pageid-17, .parent-pageid-6').length==0) {
			$('#main').fadeIn(400, function() {
				$('.sub-nav').slideDown(600);
			});	
		}
	});*/
	if ($('.page-template-work-php 	#project-sector-list').length > 0) projectSectorScrollers();
	if ($('#project-nav').length > 0) projectNavSwitcher();
	if ($('.single-project').length > 0 ) imageContentSetup();
});

$(window).resize( function(){
	matchWindowSize($('#popImage'));
	matchWindowSize($('body').find('#bkgImage'));
});


function footerSetup() {
	$("#tabs").hide();
	navHoverAnimate();
	/*setupLinkOutro();
	if ($('.home, .parent-pageid-17, .parent-pageid-6').length==0) $('#main').hide();
	if ($('.home, .single-team-member, .single-project, .parent-pageid-17, .parent-pageid-6').length==0) $('.sub-nav').hide();*/
	
	if ($('.home, .page-template-play-php').length) {
		setTimeout("fixWrap()",100);
		$(window).resize( function() {fixWrap()} );
	}
}

function fixWrap() {
	var wrap = $('#wrap'), browser = $(window);
	console.log(browser.height());
	console.log(wrap.height());
	wrap.height('');
	if (wrap.height() <  browser.height()) {
		wrap.height(browser.height());
	}
}
//////////////////////////////////////////////////////////
//
//  Navigation Menu Rollover Function
//
//////////////////////////////////////////////////////////

function navHoverAnimate() {
	$('#main-nav a[class!="active"]').mouseover(function() {
		$(this).animate({paddingLeft:'40px'},250);
	});
	$('#main-nav a[class!="active"]').mouseout(function() {
		$(this).animate({paddingLeft:'0'},100);
	});
}



//////////////////////////////////////////////////////////
//
//  Project Sector Menu Rollover Function
//
//////////////////////////////////////////////////////////

function projectNavSwitcher() {
	$('#project-nav h3 a').click(function() {
		$('.project-sector').removeClass('active');
		$('#project-nav .jScrollPaneContainer').fadeOut('200');
		$(this).parents('.project-sector').addClass('active').find('.jScrollPaneContainer').fadeIn('200');
		
		return false;
	});
}

function projectListRolloverSetup() {
	$('#project-sector-list .project-list a').mouseover(function() {
		$(this).closest('li.project-sector').find('h3 span.project-name').html('&gt;&gt; '+$(this).attr('title')).fadeIn(200);
	});
	$('#project-sector-list .project-list a').mouseout(function() {
		$(this).closest('li.project-sector').find('h3 span.project-name').hide().html("&nbsp;");
	});
}



//////////////////////////////////////////////////////////
//
//  Photo Gallery Switcher
//
//////////////////////////////////////////////////////////

function gallerySwitcherSetupOld() {
	$('#carousel a').click( function() {
		$('#carousel li').removeClass('active');
		$(this).parent().addClass('active');
		var imageObj, container=$('#image-content'), oldImageObj = $('#image-content a:first');
		imageObj=oldImageObj.clone().hide();
		imageObj.attr('href',$(this).attr('rel')).find('img').attr('src',$(this).attr('href'));
		imageObj.find('img').removeAttr('height').removeAttr('width');
		imageObj.appendTo(container);
		oldImageObj.fadeOut(400, function() {
			container.animate({height:imageObj.height()+'px'},500, function(){
				imageObj.fadeIn(200,'swing').colorbox();
			});
			$(this).remove();
		});
		return false;
	});
	
}

function gallerySwitcherSetup() {
	$('#carousel a').click( function(e) {
		e.preventDefault();
		$('#carousel li').removeClass('active');
		var activeLink = $(this),
				container=$('#image-content'), 
				imageObj = container.find('img[src='+activeLink.attr('href')+']').closest('li'),
				oldImageObj = $('#image-content li.active');
		container.css({height:oldImageObj.find('img').attr('rel')+'px'});
		activeLink.parent().addClass('active');
		oldImageObj.fadeOut(400, function() {
			container.animate({height:imageObj.find('img').attr('rel')+'px'},500, function(){
				imageObj.addClass('active').fadeIn(400,'swing').colorbox();
				oldImageObj.removeClass('active');
			});
		});
	});
	
}


//////////////////////////////////////////////////////////
//
//  Carousel Menu Scroll Functions
//
//////////////////////////////////////////////////////////

var nextLeftPos=0,
		btnNextText='View more...',
		btnPrevText='Return to start of list';

function getNextLeft(counter,items,maxVisible) {
	var totalCount = counter+maxVisible;
	while (($(items[counter+maxVisible-1]).next().length != 0) && (counter < totalCount)) {
		counter ++;
	}
	if ($(items[counter+maxVisible]).next().length == 0) {
		$("#btn_carouselScroll").fadeOut(300,function() { $(this).addClass('reverse').attr('title',btnPrevText).fadeIn(500);});
	}
	return counter;
}

function carouselScroller() {
	var cslContainer = $('#carousel'),
			cslList = $('#carousel ul'),
			cslItems = $('#carousel ul li'),
			visibleItems = Math.round(cslContainer.width()/(cslItems.first().width()+parseInt(cslItems.first().css('padding-left'))+parseInt(cslItems.first().css('padding-right'))));
	if (cslItems.length > visibleItems) {
		cslContainer.after('<a id="btn_carouselScroll" title="'+btnNextText+'" href="javascript:void(0)">&gt;</a>');
		$("#btn_carouselScroll").click(function() {
			if ($(this).hasClass('reverse')) {
				nextLeftPos = 0;
				$(this).fadeOut(300,function() {$(this).removeClass('reverse').attr('title',btnNextText).fadeIn(500);});
			} else {
				nextLeftPos = getNextLeft(nextLeftPos,cslItems,visibleItems);
			}
			var newLeft = $(cslItems[nextLeftPos]);
			cslList.animate({left:(cslList.offset().left - newLeft.offset().left)},1000);
		});
	}
}



//////////////////////////////////////////////////////////
//
//  Home Page Background Fader Functions
//
//////////////////////////////////////////////////////////

function bkgPopup() {
	/*var maxImages = 2, // <- enter maximum number of images here
			imgNum = parseInt((Math.random() * maxImages)+1),
	    imgURL = '/wp/wp-content/themes/shift/images/home/home'+imgNum+'_stretch.jpg',
	    imgURL2 = '/wp/wp-content/themes/shift/images/home/home'+imgNum+'.jpg';*/
	//$('body').append('<img id="popImage" src="'+imgURL+'" />');
	var popImage = $('#popImage'), imgURL2 = popImage.attr('src').replace("_stretch", "");
	
	//jQuery.imgpreload([imgURL],function() {
	popImage.imgpreload(function() {
		matchWindowSize(popImage);
		popImage.fadeIn(2000, function() {
			jQuery.imgpreload([imgURL2],function() {
				//$('body').css('background','#cfcbc4 url('+imgURL2+') 50% top repeat-y');
				$('body').append('<img src="'+imgURL2+'" id="bkgImage" />');
				matchWindowSize($('body').find('#bkgImage'));
				window.setTimeout("$('#popImage').fadeOut(2000,function(){$('#tabs,#captionbox').fadeIn(1000);$('.home #main-nav li a.active').animate({paddingLeft:'40px'},250);})", 2000);
			});
		});
	});
}

function playBkgPopup() {
	/*var maxImages = 2, // <- enter maximum number of images here
			imgNum = parseInt((Math.random() * maxImages)+1),
	    imgURL = '/wp/wp-content/themes/shift/images/home/home'+imgNum+'_stretch.jpg',
	    imgURL2 = '/wp/wp-content/themes/shift/images/home/home'+imgNum+'.jpg';*/
	//$('body').append('<img id="popImage" src="'+imgURL+'" />');
	var popImage = $('#popImage');
	
	//jQuery.imgpreload([imgURL],function() {
	popImage.imgpreload(function() {
		matchWindowSize(popImage);
		popImage.fadeIn(2000);
	});
}


function matchWindowSize(theObject) {
	theObject.width('100%');
	theObject.css({height:'auto',left:'0'});
	windowHeight = $(window).height();
	windowWidth = $(window).width();
	if (theObject.height() < windowHeight)	{
		theObject.height(windowHeight);
		theObject.css('width','auto');
		theObject.css('left', (windowWidth - theObject.width())/2)
	}
}

//////////////////////////////////////////////////////////
//
//  Function to horizontally scroll product sector lists
//
//////////////////////////////////////////////////////////

function projectSectorScrollers() {
	$('.project-list li:nth-child(10)').parent().each( function() {
			$(this).wrap('<div class="scroller" />');
			$(this).closest('.scroller').append('<a href="#" class="scrollBtn">&gt;</a>');
			var listSize = $(this).find('li').length,
					newLength = (listSize*55)+((listSize-1)*3);
			$(this).css('width',newLength+'px');
			$(this).closest('.scroller').find('.scrollBtn').click( function(e) {
				e.preventDefault();
				var thisBtn = $(this),
						sList = $(this).parent().find('ul'),
						sOffset = 519 - sList.width();
				if(thisBtn.hasClass('scrolled')) {
					sList.animate({marginLeft:0}, 500, function() {
						thisBtn.removeClass('scrolled');
					});
				} else {
					sList.animate({marginLeft:sOffset+'px'}, 500, function() {
						thisBtn.addClass('scrolled');
					});
				}
			});
		});
}

//////////////////////////////////////////////////////////
//
//  Function to add magnify button to images
//
//////////////////////////////////////////////////////////

function imageContentSetup() {
	$('#gallery #image-content li').each( function() {    
		$(this).find('a').append('<div class="btnMagnify">Zoom</a>');
		$(this).find('a').colorbox({rel:'project-gallery', current:"{current}/{total}"});
	});
}

//////////////////////////////////////////////////////////
//
//  Misc Functions
//
//////////////////////////////////////////////////////////

function setupLinkOutro() {
	$('#main-nav a, .sub-nav a, #content > #project-sector-list a').click( function() {
		var hrefLoc = $(this).attr('href');
		$('#wrap').fadeOut(200, function() {
			window.location = hrefLoc;
		});
		return false;
	});
}

function popUp(URL) {
	day = new Date();
	id = day.getTime();
	eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=254,height=154');");
}

var addthis_config = {
	ui_click: true
}