<?php header("HTTP/1.1 404 Not Found"); ?>
<?php header("Status: 404 Not Found"); ?>
<?php get_header(); ?>
<div id="main">
	<div id="content">
		<div id="content">

				<img width="520" height="150" src="http://www.shiftarchitecture.ca/wp/wp-content/uploads/2-BASIC-CONTACT.jpg" class="attachment-post-thumbnail wp-post-image" alt="2 BASIC CONTACT" title="2 BASIC CONTACT" />
				<div id="entry" class="sroll-bar">
					<div id="pane" class="scroll-pane">
			
			
			<h2>File or page not found.</h2>
			
			<p>We've recently made changes to our website and the page you are looking for might have been deleted or moved. Please <a href="<?php bloginfo('url'); ?>">visit our home page instead</a>. </p>

			<p>Sorry for the inconvenience.</p>
		</div><!-- entry -->
	</div><!-- pane -->
	</div>
<?php get_sidebar(); ?>
</div><!-- end of main div -->
<?php get_footer(); ?>
