<?php
/* Template Name: Activity */
?>
<?php get_header(); ?>

<div id="main">
	<div id="content">
  	<?php echo get_the_post_thumbnail( $post->ID, 'post-thumbnail' ); ?>
    <div id="entry" class="sroll-bar">
      <div id="pane" class="scroll-pane">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?php the_content();?>
        <?php endwhile; endif; ?>
        <?php
			$args=array(
				'orderby' => 'date',
				'order' => 'DESC'
			);
	
			$wp_query = new WP_Query($args); ?>
      
        <?php	if( have_posts() ) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
        <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
          <h3><?php the_title(); ?> - <?php the_time('F jS, Y') ?></h3>
            <?php the_content(); ?>
        </div>
        <?php endwhile; ?>
        <?php endif;  ?>
      </div>
      <!-- entry --> 
    </div>
    <!-- pane --> 
  </div>
  <?php get_sidebar(); ?>
</div>
<!-- end of main-->
<?php get_footer(); ?>
