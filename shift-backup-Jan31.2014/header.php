<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php if (is_front_page()) : ?><?php bloginfo('name'); ?>
<?php else : ?><?php wp_title('', 'false'); ?> - <?php bloginfo('name'); ?>
<?php endif; ?></title>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="shortcut icon" type="image/x-icon" href="http://www.shiftarchitecture.ca/favicon.ico">

<!--[if IE]>
   <style type="text/css">
   #captionbox {
       background: url(http://www.shiftarchitecture.ca/wp/wp-content/themes/shift/images/captionbox-microsoft.png);
    } 
#footer {
	background: url(http://www.shiftarchitecture.ca/wp/wp-content/themes/shift/images/footer-microsoft.png);
}
    </style>
<![endif]-->

<?php wp_head(); ?>
<?php if(is_page_template('play.php')) {?>
<script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/innerfade.js"></script>
<script type="text/javascript">
 $(document).ready(
		function(){
	$('.fade ul').innerfade({
				speed: 1000,
				timeout: 5000,
				type: 'sequence',
				containerheight: '430px'
			});
});
</script>
<?php }?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-30932939-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body <?php body_class(); ?>>
<?php 
global $caption;
if(is_page('home')) $caption = get_home_image(); 
if(is_page('play')) echo '<img id="popImage" src="http://www.shiftarchitecture.ca/wp/wp-content/themes/shift/images/play_bg.jpg" />';
?>
<div id="wrap">	
<script> 
	/*if ($('.home, .single-team-member').length==0) $('#wrap').hide();*/
</script>
 <div id="logo"><a href="<?php bloginfo('url'); ?>"><span class="hide"><?php bloginfo('name'); ?></span></a></div>
  <div id="header">
	<div id="subscribe"><a href="javascript:popUp('http://www.shiftarchitecture.ca/wp/wp-content/themes/shift/newsletter.html')">Subscribe</a></div>
	<div id="client"><a href="<?php bloginfo('url');?>/client-zone/"><span class="hide">Client Zone</span></a></div>
	<div id="bookmark"><a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4bbbc9f50f110e7f">Bookmark and Share</a><script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4bbbc9f50f110e7f"></script>
		<a class="contact" href="mailto:feedback@shiftarchitecture.ca">Contact Us</a></div>
  </div>