jQuery( document ).ready( function( $ ) {
	var contentBox = $( '#pane' ),
			contentBoxes = $('.listpane');
	var manufacturerSelect = $( '#select-manufacturer' );
	if( 1 === contentBox.length ) {
		$( contentBox ).jScrollPane( {
			scrollbarWidth : 10,
			wheelSpeed: 10,
			animateTo : true
			} );
		if( 1 === contentBox.length ) {
			$( manufacturerSelect ).change( function() {
				contentBox[0].scrollTo( '#' + $( this ).val() );
				} );
		}
	}
	if(contentBoxes.length != 0) {
		$( contentBoxes ).jScrollPane( {
			scrollbarWidth : 10,
			wheelSpeed: 10,
			animateTo : true
			} );
	}
	
	$('#project-nav .jScrollPaneContainer').hide();
	$('#project-nav .active .jScrollPaneContainer').show();
} );