<?php get_header(); ?>

<div id="main">
	<div id="content">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<?php the_post_thumbnail(); ?>
		
		<div id="entry" class="sroll-bar">
			<div id="pane" class="scroll-pane">
				<?php the_content();?>
			</div><!-- entry -->
		</div><!-- pane -->

	 <?php endwhile; endif; ?>
	</div>
<?php get_sidebar(); ?>
</div><!-- end of main-->
<?php get_footer(); ?>
