<?php
/* Template Name: Play */
?>

<?php get_header(); ?>
<div id="main">
	<div id="content" class="fade">
		<ul>
	<?php if (have_posts()) : while (have_posts()) : the_post(); 	

		$args = array(
			'post_type' => 'attachment',
			'numberposts' => -1,
			'post_status' => null,
			'post_parent' => $post->ID
		);
		$attachments = get_posts($args);
		if ($attachments) {
			foreach ($attachments as $attachment) {
				print "\n";
				echo '<li>';
				echo wp_get_attachment_image($attachment->ID, 'full');
				echo '<p>';
				echo apply_filters('the_title', $attachment->post_title);
				echo '</p></li>';
			}
		}

	 endwhile; endif; ?>
	</ul>
	</div>
<?php get_sidebar(); ?>
</div><!-- end of main-->
<script>fixWrap();</script>
<?php get_footer(); ?>
