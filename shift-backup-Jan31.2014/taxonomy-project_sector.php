<?php 
/* Template Name: Work */


get_header(); ?>

<div id="main">
  <div id="content">
<?php
        query_posts( 'post_type=projects' );
        get_template_part( 'loop' ); // looks for loop.php which is specific to Twenty Ten
        wp_reset_query();
      ?>


  </div>
  <?php get_sidebar(); ?>
</div>
<!-- end of main-->
<?php get_footer(); ?>
