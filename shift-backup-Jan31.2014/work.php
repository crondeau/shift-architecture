<?php 
/* Template Name: Work */


get_header(); ?>

<div id="main">
  <div id="content">
    <?php get_projects(true); ?>
  </div>
  <script>projectListRolloverSetup();</script>
  <?php get_sidebar(); ?>
</div>
<!-- end of main-->
<?php get_footer(); ?>
