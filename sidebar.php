<div id="sidebar">
	  <ul id="main-nav">
		<li id="home"><a href="<?php echo home_url(); ?>"<?php if(is_front_page()) {echo' class="active"';}?>>HOME</a></li>
		<li id="studio"><a href="<?php echo home_url(); ?>/studio/"<?php if(is_page(6) || (is_child(6)) || ($post->post_type == 'team-member') || in_category('news')) {echo' class="active"';} ?>>STUDIO</a></li>
		<li id="work"><a href="<?php echo home_url(); ?>/work/"<?php if(is_page(17) || (is_child(17)) || ($post->post_type == 'project')) {echo' class="active"';} ?>>WORK</a></li>
		<li id="contact"><a href="<?php echo home_url(); ?>/contact/"<?php if(is_page(21)) {echo' class="active"';} ?>>CONTACT</a></li>
	 </ul>
	
	<?php if(in_category('news') || is_page(6) || is_child(6) || (get_post_type() == 'team-member')) { // Display studio sub-pages ?>
		
		<ul id="studio-nav" class="sub-nav">
			<li id="overview"><a href="<?php echo home_url(); ?>/studio/"<?php if(is_page(6)) {echo' class="active"';}?>>Overview</a></li>
			<li id="stewardship"><a href="<?php echo home_url(); ?>/studio/stewardship/"<?php if(is_page(9)) {echo' class="active"';} ?>>Stewardship</a></li>
			<li id="culture"><a href="<?php echo home_url(); ?>/studio/culture/"<?php if(is_page(11)) {echo' class="active"';} ?>>Culture</a></li>
			<li id="team"><a href="<?php echo get_first_post_link('team-member'); ?>"<?php if(is_page(13) || is_child(13) || (get_post_type() == 'team-member')) {echo' class="active"';} ?>>Team</a></li>

		 </ul>
	
	<?php } ?>
	
	<?php if(is_page(17) || is_child(17) || ( get_post_type() == 'project' )) { // Display work sub-pages ?>
		
		<ul id="work-nav" class="sub-nav">
			<li id="map"><a href="<?php echo home_url(); ?>/work/"<?php if(is_page(17)) {echo' class="active"';}?>>Project Map</a></li>
			<li id="sector"><a href="<?php echo get_first_post_link('project'); ?>"<?php if(is_page(53) || ( get_post_type() == 'project' )) {echo' class="active"';} ?>>Project by Sector</a></li>
			<?php /*?><li id="dev"><a href="<?php echo home_url(); ?>/work/dev/"<?php if(is_page(55)) {echo' class="active"';} ?>>In Development</a></li><?php */?>
			
		 </ul>
	
	<?php } ?>
	
	
	
</div>


