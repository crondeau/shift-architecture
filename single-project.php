
<?php get_header(); ?>
<div id="main">
	<div id="content">	
	
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
  	<div id="project-nav" class="sroll-bar">
        <?php get_projects(false); ?>
    </div>
    <div id="gallery">
      <div id="carousel">
					<?php get_post_images(true); ?>
      </div>
      <script>carouselScroller(); gallerySwitcherSetup();</script>
			<div id="image-content"><?php 
				get_post_images();
			?></div>
		</div>
		
    <div class="meta">
			<h2><?php the_title();?></h2>
      <dl id="project-info-list">
			<?php
				$role = get_post_meta($post->ID, "project-role", true);
				$firm = get_post_meta($post->ID, "project-firm", true);
				$location = get_post_meta($post->ID, "project-location", true);
			if($role!='') { echo '<dt>Role:</dt><dd>'.$role.'</dd>'; }
			if($firm!='') { echo '<dt>Architecture Firm:</dt><dd>'.$firm.'</dd>'; }
			if($location!='') { echo '<dt>Location:</dt><dd>'.$location.'</dd>'; }
			?>
			
      </dl>
		</div>
    
		<div id="entry">
			<div class="single-content">
			<?php the_content();?>
			</div>
		</div><!-- pane -->

	 <?php endwhile; endif; ?>
	</div>
<?php get_sidebar(); ?>
</div><!-- end of main-->
<?php get_footer(); ?>
