<?php
/* Template Name: Team */

/* I think that we need to grab the children of the team page.
Display the first team member and have a scroller above with the thumbnails of the other members.
*/
?>

<?php get_header(); ?>
<div id="main">
	<div id="content">
		
		
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>		
  
  
		<ul id="carousel">
    <?php get_team_pages(13,$post->ID); ?>
		</ul>
		<?php if (!is_page('team')) { ?>
		<div class="meta">
			<?php the_post_thumbnail(); ?>
			<h2><?php the_title();?></h2>
			<p><?php $key="Accreditation"; echo get_post_meta($post->ID, $key, true); ?></p>
			<p><?php $key="Job-title"; echo get_post_meta($post->ID, $key, true); ?></p>
		</div>
    <?php } ?>
		<div id="entry" class="sroll-bar">
			<div id="pane" class="scroll-pane">
				<?php the_content();?>
			</div><!-- entry -->
		</div><!-- pane -->

	 <?php endwhile; endif; ?>
	</div>
<?php get_sidebar(); ?>
</div><!-- end of main-->
<?php get_footer(); ?>
